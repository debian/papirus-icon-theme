Source: papirus-icon-theme
Section: x11
Priority: optional
Maintainer: Debian Desktop Theme Team <team+desktop-themes-team@tracker.debian.org>
Standards-Version: 4.7.0
Build-Depends:
 debhelper-compat (= 13),
Rules-Requires-Root: no
Homepage: https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
Vcs-Git: https://salsa.debian.org/debian/papirus-icon-theme.git
Vcs-Browser: https://salsa.debian.org/debian/papirus-icon-theme

Package: papirus-icon-theme
Architecture: all
Depends:
 hicolor-icon-theme,
 ${misc:Depends},
Recommends:
# Some buggy software needs it to parse SVG but failed to declare dependency
# itself, so we accomodate the status quo and add this recommendation here.
# See https://bugs.debian.org/982901 and https://bugs.debian.org/1029056
 librsvg2-common,
# Declare the de facto conflict with epapirus-icon-theme package before
# we properly split the package out as requested by upstream
# and https://bugs.debian.org/1052160
Breaks:
 epapirus-icon-theme,
Provides:
 epapirus-icon-theme,
Replaces:
 epapirus-icon-theme,
Suggests:
# Used in postinst if exists.
 gtk-update-icon-cache,
Description: Papirus open source icon theme for Linux
 Papirus is a SVG-based icon theme, drawing inspiration from Material Design
 and flat design.
 .
 This package contains the following icon themes:
  - Papirus
  - Papirus-Dark
  - Papirus-Light
  - ePapirus
  - ePapirus-Dark
